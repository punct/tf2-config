"GameMenu"
{

	////////////////////
	/// MAIN BUTTONS ///
	////////////////////

	"resume"
	{
		"label" "RESUME GAME"
		"command" "ResumeGame"
		"OnlyInGame" "1"
	}
	"quickplay_menu"
	{
		"label" "START PLAYING" 
		"command" "quickplay"
		"OnlyAtMenu" "1"
	}
	"quickplay_ingame"
	{
		"label" "NEW GAME" 
		"command" "quickplay"
		"OnlyInGame" "1"
	}
	"serverbrowser_menu"
	{
		"label" "BROWSE SERVERS" 
		"command" "OpenServerBrowser"
		"OnlyAtMenu" "1"
	} 
	"serverbrowser_ingame"
	{
		"label" "CHANGE SERVERS" 
		"command" "OpenServerBrowser"
		"OnlyInGame" "1"
	}
	"separator1"
	{
		"label" ""
		"command" ""
	}
	"loadout"
	{
		"label" "ITEMS"
		"command" "engine open_charinfo"
	}
	"separator2"
	{
		"label" ""
		"command" ""
	}
	"scoreboard6v6"
	{
		"label" "6v6 SCOREBOARD"
		"command" "engine cl_hud_minmode 1"
	}
	"scoreboard12v12"
	{
		"label" "12v12 SCOREBOARD"
		"command" "engine cl_hud_minmode 0"
	}
	"separator3"
	{
		"label" ""
		"command" ""
	}
	"disconnect"
	{
		"label" "DISCONNECT"
		"command" "engine disconnect"
		"OnlyInGame"	"1"
	}
	"ragequit"
	{
		"label" "RAGEQUIT"
		"command" "engine quit"
	}

	///////////////////
	/// SUB BUTTONS ///
	///////////////////


	"SettingsButton"
	{
		"label" ""
		"command" "OpenOptionsDialog"
		"tooltip" "SETTINGS"
	}
	"TF2SettingsButton"
	{
		"label" ""
		"command" "opentf2options"
		"tooltip" "ADVANCED SETTINGS"
	}

	"CreateServerButton"
	{
		"label" ""
		"command" "OpenCreateMultiplayerGameDialog"
		"OnlyAtMenu" "1"
		"tooltip" "CREATE SERVER"
	}
	"AchievementsButton"
	{
		"label" ""
		"command" "OpenAchievementsDialog"
		"tooltip" "ACHIEVEMENTS"
	}
	"CommentaryButton"
	{
		"label" ""
		"command" "OpenLoadSingleplayerCommentaryDialog"
		"tooltip" "DEVELOPER COMMENTARY"
	}
	"CoachPlayersButton"
	{
		"label" ""
		"command" "engine cl_coach_toggle"
		"tooltip" "COACH PLAYER"
	}
	"ReportBugButton"
	{
		"label" ""
		"command" "engine bug"
		"tooltip" "REPORT BUG"
	}


	"CallVoteButton"
	{
		"label"			""
		"command"		"callvote"
		"OnlyInGame"	"1"
		"tooltip" "CALL VOTE"
	}
	"MutePlayersButton"
	{
		"label"			""
		"command"		"OpenPlayerListDialog"
		"OnlyInGame"	"1"
		"tooltip" "MUTE PLAYERS"
	}
	"RequestCoachButton"
	{
		"label"			""
		"command"		"engine cl_coach_find_coach"
		"OnlyInGame"	"1"
		"tooltip" "REQUEST COACH"
	}
}