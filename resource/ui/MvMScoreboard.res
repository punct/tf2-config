"Resource/UI/MvMScoreboard.res"
{		
	"WaveStatusPanel"
	{
		"ControlName"		"CWaveStatusPanel"
		"fieldName"			"WaveStatusPanel"
		"xpos"				"0"
		"ypos"				"16"
		"zpos"				"0"
		"wide"				"400"
		"tall"				"67"
		"visible"			"1"
		"enabled"			"1"
		
		"verbose"			"1"
	}
	
	"PopFileLabel"
	{
		"ControlName"	"CExLabel"
		"fieldName"		"PopFileLabel"
		"font"			"ScoreboardMedium"
		"labelText"		"%popfile%"
		"textAlignment"	"west"
		"xpos"			"190"
		"ypos"			"347"
		"wide"			"290"
		"tall"			"20"
		"fgcolor"		"tanlight"
	}

	"MannUpContainer"
	{
		"ControlName"	"EditablePanel"
		"fieldName"		"MannUpContainer"
		"xpos"			"400"
		"ypos"			"20"
		"wide"			"200"
		"tall"			"100"
		"visible"		"0"
		
		"MannUpLabel"
		{
			"ControlName"	"CExLabel"
			"fieldName"		"MannUpLabel"
			"font"			"HudFontSmallBold"
			"labelText"		"Mann-Up : On"
			"textAlignment" "center"
			"xpos"			"0"
			"ypos"			"0"
			"wide"			"200"
			"tall"			"20"
			"fgcolor"		"tanlight"
		}
		
		"SquadSurplusLabel"
		{
			"ControlName"	"CExLabel"
			"fieldName"		"SquadSurplusLabel"
			"font"			"HudFontSmallBold"
			"labelText"		"Squad Surplus : x6"
			"textAlignment" "center"
			"xpos"			"0"
			"ypos"			"12"
			"wide"			"200"
			"tall"			"20"
			"fgcolor"		"tanlight"
		}
	}
	
	"PlayerListBackground"
	{
		"ControlName"		"ScalableImagePanel"
		"fieldName"		"PlayerListBackground"
		"xpos"			"0"
		"ypos"			"75"
		"zpos"			"-1"
		"wide"			"370"
		"tall"			"150"
		"visible"		"1"
		"enabled"		"1"
		"image"			"../hud/color_panel_brown"
		
		"scaleImage"		"1"
		
		"src_corner_height"	"23"				// pixels inside the image
		"src_corner_width"	"23"
	
		"draw_corner_width"	"3"				// screen size of the corners ( and sides ), proportional
		"draw_corner_height" 	"3"	
	}
	
	"MvMPlayerList"
	{
		"ControlName"	"SectionedListPanel"
		"fieldName"		"MvMPlayerList"
		"xpos"			"0"
		"ypos"			"79"
		"wide"			"370"
		"tall"			"150"
		"pinCorner"		"0"
		"visible"		"1"
		"enabled"		"1"
		"tabPosition"	"0"
		"autoresize"	"3"
		"linespacing"	"22"
		"textcolor"		"White"
	}
	
	"CreditStatsContainer"
	{
		"ControlName"	"EditablePanel"
		"fieldName"		"CreditStatsContainer"
		"xpos"			"0"
		"ypos"			"230"
		"wide"			"370"
		"tall"			"185"
		"visible"		"1"
		
		"CreditStatsBackground"
		{
			"ControlName"		"ScalableImagePanel"
			"fieldName"		"CreditStatsBackground"
			"xpos"			"0"
			"ypos"			"0"
			"zpos"			"-1"
			"wide"			"370"
			"tall"			"115"
			"autoResize"	"0"
			"pinCorner"		"0"
			"visible"		"1"
			"enabled"		"1"
			"image"			"../HUD/color_panel_brown"

			"src_corner_height"	"23"				// pixels inside the image
			"src_corner_width"	"23"
		
			"draw_corner_width"	"3"				// screen size of the corners ( and sides ), proportional
			"draw_corner_height" 	"3"	
		}
		
		"CreditsLabel"
		{
			"ControlName"	"CExLabel"
			"fieldName"		"CreditsLabel"
			"font"			"HudFontMediumSmall"
			"labelText"		"#TF_PVE_Currency"
			"textAlignment" "north-west"
			"xpos"			"8"
			"ypos"			"8"
			"wide"			"200"
			"fgcolor"		"tanlight"
			"visible"		"0"
		}
		
		"PreviousWaveCreditInfoPanel"
		{
			"ControlName"	"CCreditDisplayPanel"
			"fieldName"		"PreviousWaveCreditInfoPanel"
			"xpos"			"10"
			"ypos"			"7"
			"wide"			"170"
			"tall"			"60"
			"wide"			"200"
			"visible"		"1"
		}
		
		"TotalGameCreditInfoPanel"
		{
			"ControlName"	"CCreditDisplayPanel"
			"fieldName"		"TotalGameCreditInfoPanel"
			"xpos"			"190"
			"ypos"			"7"
			"wide"			"170"
			"tall"			"60"
			"wide"			"200"
			"visible"		"1"
		}
		
		"PreviousWaveCreditSpendPanel"
		{
			"ControlName"	"CCreditSpendPanel"
			"fieldName"		"PreviousWaveCreditSpendPanel"
			"xpos"			"10"
			"ypos"			"52"
			"wide"			"170"
			"tall"			"60"
			"wide"			"200"
			"visible"		"1"
		}
		
		"TotalGameCreditSpendPanel"
		{
			"ControlName"	"CCreditSpendPanel"
			"fieldName"		"TotalGameCreditSpendPanel"
			"xpos"			"190"
			"ypos"			"52"
			"wide"			"170"
			"tall"			"60"
			"wide"			"200"
			"visible"		"1"
		}
	}
}
